class profile::base {

  #the base profile should include component modules that will be on all nodes
  
  file { '/etc/puppetlabs/facter':
    ensure => directory,
  }

  file { '/etc/puppetlabs/facter/facter.d':
    ensure => directory,
  }

  package { 'bash-completion':
    ensure => installed,
  }

  packge { 'tree':
    ensure => installed,
  }
}
